#!/bin/bash

# Check if sudo
if [ "$EUID" -ne 0 ]; then
  echo "Please run as root"
  exit
fi

# Get the scripts folder
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Copy service file to systemd
cp $DIR/reverse-ssh-tunnel.service /etc/systemd/system/

# Reload the service daemon files
systemctl daemon-reload

# Add to auto-start
systemctl enable reverse-ssh-tunnel.service 

# Start it now
systemctl start reverse-ssh-tunnel.service 

# Print out the status
systemctl status reverse-ssh-tunnel.service
